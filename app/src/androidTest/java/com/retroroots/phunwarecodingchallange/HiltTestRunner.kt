package com.retroroots.phunwarecodingchallange

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import dagger.hilt.android.testing.HiltTestApplication

//It's being used by all of the instrumentations test, despite the results of the code analyzer.
class HiltTestRunner: AndroidJUnitRunner()
{
    override fun newApplication(cl: ClassLoader?,
                                className: String?,
                                context: Context?): Application
    {
        return super.newApplication(cl, HiltTestApplication::class.java.name, context)
    }
}