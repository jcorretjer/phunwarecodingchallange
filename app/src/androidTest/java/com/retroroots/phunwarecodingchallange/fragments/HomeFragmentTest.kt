package com.retroroots.phunwarecodingchallange.fragments

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.retroroots.phunwarecodingchallange.R
import com.retroroots.phunwarecodingchallange.data.common.StarWarsChapter
import com.retroroots.phunwarecodingchallange.data.remote.api.StarWarsApi
import com.retroroots.phunwarecodingchallange.ui.main.MainActivity
import com.retroroots.phunwarecodingchallange.util.dateFormatter.DateFormat
import com.retroroots.phunwarecodingchallange.util.dateFormatter.DateFormatter
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class HomeFragmentTest
{
    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var starWarsApi: StarWarsApi

    @Before
    fun init()
    {
        launchActivity<MainActivity>()

        hiltRule.inject()
    }

    @Test
    fun validateRecyclerViewDataTest()
    {
        assertRecyclerViewDataMatchesWithAnItem()
    }

    private fun assertRecyclerViewDataMatchesWithAnItem()
    {
        var item: StarWarsChapter

        runBlocking {
            val response = starWarsApi.getChapters()

            item = response.body()!![0]
        }

        item.apply {
            val location = "$locationLine1, $locationLine2"

            //location
            onView(withId(R.id.recVw)).check(matches(hasDescendant(withText(location))))

            val date = DateFormatter.format(
                DateFormat(
                    currentFormat = "yyyy-MM-dd HH:mm:ss",
                    date = date,
                    outputFormat = "MMM dd, yyyy h:mm z"
                )
            )

            //date
            onView(withId(R.id.recVw)).check(matches(hasDescendant(withText(date))))

            //desc
            onView(withId(R.id.recVw)).check(matches(hasDescendant(withText(description))))

            //title
            onView(withId(R.id.recVw)).check(matches(hasDescendant(withText(title))))
        }
    }
}