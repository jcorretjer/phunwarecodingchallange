package com.retroroots.phunwarecodingchallange.navigation

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.retroroots.phunwarecodingchallange.R
import com.retroroots.phunwarecodingchallange.ui.home.HomeAdapter
import com.retroroots.phunwarecodingchallange.ui.main.MainActivity
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
class HomeNavigationTest
{
    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Before
    fun init()
    {
        launchActivity<MainActivity>()

        hiltRule.inject()
    }

    @Test
    fun onStartupNavigationTest()
    {
        onView(withId(R.id.recVw)).check(matches(isDisplayed()))
    }

    @Test
    fun navigateToDetailFromHomeText()
    {
        onView(withId(R.id.recVw)).perform(actionOnItemAtPosition<HomeAdapter.HomeViewHolder>(0, click()))

        onView(withId(R.id.chapterDateTxtVw)).check(matches(isDisplayed()))
    }
}