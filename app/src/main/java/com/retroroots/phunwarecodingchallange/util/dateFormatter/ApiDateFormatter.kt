package com.retroroots.phunwarecodingchallange.util.dateFormatter

interface ApiDateFormatter
{
    fun format(dateFormat: DateFormat) : String
}