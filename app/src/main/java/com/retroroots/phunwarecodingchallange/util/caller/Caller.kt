package com.retroroots.phunwarecodingchallange.util.caller

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import com.retroroots.phunwarecodingchallange.R

abstract class Caller
{
    /**
     * Launches dialing intent
     * @param to Value used to prepopulate the phone number text of the dialer
     */
    fun makeFromDialer(context: Context, to: String)
    {
        try
        {
            val intent = Intent(Intent.ACTION_DIAL).apply {
                data = Uri.parse("tel:$to")
            }

            context.startActivity(intent)
        }

        catch (e: Exception)
        {
            Toast.makeText(context, "${context.resources.getString(R.string.callingFailedErrorTxt)}. Error: ${e.message}", Toast.LENGTH_LONG).show()
        }
    }
}