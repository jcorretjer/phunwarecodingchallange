package com.retroroots.phunwarecodingchallange.util.dateFormatter

import java.util.*

data class DateFormat(
    /**
     * Must be a valid date format based on SimpleDateFormat and DateTimeFormatter.
     */
    var outputFormat: String = "",

    /**
     * Must be a valid date format based on SimpleDateFormat.
     */
    var currentFormat: String = "",

    /**
     * Must be a valid date format based on SimpleDateFormat and DateTimeFormatter.
     */
    var date: String = "",
    var timeZone: TimeZone = TimeZone.getDefault()
)
