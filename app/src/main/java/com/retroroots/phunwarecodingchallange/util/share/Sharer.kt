package com.retroroots.phunwarecodingchallange.util.share

import android.content.Context
import android.content.Intent
import com.retroroots.phunwarecodingchallange.R

abstract class Sharer
{
    /**
     * Launch sharing intent.
     *
     * @param text Value to share
     */
    fun sendText(context: Context, text: String)
    {
        val intent = Intent(Intent.ACTION_SEND).apply {
            putExtra(Intent.EXTRA_TEXT, text)

            type = "text/plain"
        }

        context.startActivity(Intent.createChooser(intent, context.resources.getString(R.string.shareTitleTxt)))
    }
}