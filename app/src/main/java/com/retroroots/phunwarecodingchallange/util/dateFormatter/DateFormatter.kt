package com.retroroots.phunwarecodingchallange.util.dateFormatter

import java.lang.Exception
import java.text.SimpleDateFormat
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*

object DateFormatter : ApiDateFormatter
{
    annotation class ExceptionMessages
    {
        companion object
        {
            const val EMPTY_CURRENT_FORMAT = "Current format must not be empty"
            const val EMPTY_OUTPUT_FORMAT = "Output format must not be empty"
            const val EMPTY_DATE = "Date must not be empty"
        }
    }

    /**
     * If api 26+, formats with DateTimeFormatter. Else, SimpleDateFormat
     *
     * @return Formatted date.
     */
    override fun format(dateFormat: DateFormat): String
    {
        if(dateFormat.currentFormat.isEmpty())
        {
            throw Exception(ExceptionMessages.EMPTY_CURRENT_FORMAT)
        }

        if(dateFormat.outputFormat.isEmpty())
        {
            throw Exception(ExceptionMessages.EMPTY_OUTPUT_FORMAT)
        }

        if(dateFormat.date.isEmpty())
        {
            throw Exception(ExceptionMessages.EMPTY_DATE)
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            val dateFormatted = ZonedDateTime.parse(dateFormat.date, DateTimeFormatter.ISO_DATE_TIME).withZoneSameInstant(ZoneId.systemDefault())

            return DateTimeFormatter.ofPattern(dateFormat.outputFormat).format(dateFormatted)
        }

        else
        {
            //Format with the pattern the date originally comes in
            val parser = SimpleDateFormat(dateFormat.currentFormat, Locale.getDefault())

            parser.timeZone = dateFormat.timeZone

            //The formatter kept throwing errors because of the 'T' and the 'Z'
            val dateFormatted = parser.parse(dateFormat.date.replace("T", " ").replace("Z", ""))

            //Format using the pattern we want
            return SimpleDateFormat(dateFormat.outputFormat, Locale.getDefault()).format(dateFormatted!!)
        }
    }
}