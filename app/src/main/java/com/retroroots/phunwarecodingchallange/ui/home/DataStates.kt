package com.retroroots.phunwarecodingchallange.ui.home


enum class DataStates
{
    SUCCESS,
    ERROR;
}