package com.retroroots.phunwarecodingchallange.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.retroroots.phunwarecodingchallange.R
import com.retroroots.phunwarecodingchallange.data.common.StarWarsChapter
import com.retroroots.phunwarecodingchallange.util.dateFormatter.DateFormat
import com.retroroots.phunwarecodingchallange.util.dateFormatter.DateFormatter

class HomeAdapter(private val context: Context, private val clickedListener: OnItemClickedListener): ListAdapter<StarWarsChapter, HomeAdapter.HomeViewHolder>(DiffCallback())
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder
    {
        return HomeViewHolder(
            LayoutInflater.from(parent.context)
            .inflate(R.layout.item_layout, parent, false))
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int)
    {
        holder.apply {

            getItem(position).apply {

                Glide.with(context)
                    .load(image)
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_nomoon)
                    .error(R.drawable.placeholder_nomoon)
                    .fallback(R.drawable.placeholder_nomoon)
                    .into(cardBackgroundImg)

                cardDateTxtVw.text = DateFormatter.format(DateFormat(currentFormat = "yyyy-MM-dd HH:mm:ss", date = date, outputFormat = "MMM dd, yyyy h:mm z"))

                cardDescTxtVw.text = description

                val location = "$locationLine1, $locationLine2"

                cardLocationTxtVw.text = location

                cardTitleTxtVw.text = title
            }
        }
    }

    inner class HomeViewHolder(view:  View) : RecyclerView.ViewHolder(view)
    {
        val cardBackgroundImg: ImageView = view.findViewById(R.id.cardBackgroundImg)

        val cardDateTxtVw: TextView = view.findViewById(R.id.cardDateTxtVw)

        val cardDescTxtVw: TextView = view.findViewById(R.id.cardDescTxtVw)

        val cardLocationTxtVw: TextView = view.findViewById(R.id.cardLocationTxtVw)

        val cardTitleTxtVw: TextView = view.findViewById(R.id.cardTitleTxtVw)

        init {
            view.setOnClickListener{
                val position = adapterPosition

                if(position != RecyclerView.NO_POSITION)
                    clickedListener.onItemClicked(getItem(position))
            }
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<StarWarsChapter>()
    {
        override fun areItemsTheSame(oldItem : StarWarsChapter,
                                     newItem : StarWarsChapter
        ): Boolean
        {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem : StarWarsChapter,
                                        newItem : StarWarsChapter
        ): Boolean
        {
            return oldItem == newItem
        }
    }

    interface OnItemClickedListener
    {
        fun onItemClicked(starWarsChapter: StarWarsChapter)
    }
}