package com.retroroots.phunwarecodingchallange.ui.main

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PhuAppApplication: Application()