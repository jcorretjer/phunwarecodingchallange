package com.retroroots.phunwarecodingchallange.ui.detail

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.NavigationUI
import com.bumptech.glide.Glide
import com.retroroots.phunwarecodingchallange.R
import com.retroroots.phunwarecodingchallange.util.caller.CallingHelper
import com.retroroots.phunwarecodingchallange.util.dateFormatter.DateFormat
import com.retroroots.phunwarecodingchallange.util.dateFormatter.DateFormatter
import com.retroroots.phunwarecodingchallange.util.share.SharingHelper

class DetailFragment : Fragment(R.layout.fragment_detail)
{
    private val args by navArgs<DetailFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        //Toolbar
        (requireActivity() as AppCompatActivity).apply {
            val toolbar = view.findViewById<Toolbar>(R.id.detailToolbar)

            toolbar.title = ""

            setSupportActionBar(toolbar)

            NavigationUI.setupActionBarWithNavController(this, findNavController())
        }

        //Display data
        args.starWarsChapter.apply {
            Glide.with(requireContext())
                .load(image)
                .centerCrop()
                .placeholder(R.drawable.placeholder_nomoon)
                .error(R.drawable.placeholder_nomoon)
                .fallback(R.drawable.placeholder_nomoon)
                .into(view.findViewById(R.id.chapterImgVw))

            view.findViewById<TextView>(R.id.chapterDateTxtVw).text = DateFormatter.format(DateFormat(currentFormat = "yyyy-MM-dd HH:mm:ss", date = date, outputFormat = "MMM dd, yyyy h:mm z"))

            view.findViewById<TextView>(R.id.chapterDescTxtVW).text = description

            val location = "$locationLine1, $locationLine2"

            view.findViewById<TextView>(R.id.chapterLocationTxtVW).text = location

            view.findViewById<TextView>(R.id.chapterTitleTxtVW).text = title
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater)
    {
        inflater.inflate(R.menu.detail_toolbar_items, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        val starWarsChapter = args.starWarsChapter

        return when (item.itemId)
        {
            R.id.callBtn ->
            {
                CallingHelper.makeFromDialer(requireContext(), starWarsChapter.phone ?: "")

                true
            }

            R.id.shareBtn ->
            {
                SharingHelper.sendText(requireContext(), starWarsChapter.title)

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }
}