package com.retroroots.phunwarecodingchallange.ui.home

import androidx.lifecycle.*
import com.retroroots.phunwarecodingchallange.data.common.StarWarsChapter
import com.retroroots.phunwarecodingchallange.data.local.room.LocalCacheRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val localCacheRepo : LocalCacheRepo) : ViewModel()
{
    //Dictates the current state of the data. With this you can get a better understanding as to why the data came back empty without assuming something wen't wrong.
    private val mutableDataState : MutableLiveData<DataStates> = MutableLiveData(DataStates.SUCCESS)

    private val dataState: LiveData<DataStates> = mutableDataState

    private val chaptersMutable = MutableLiveData<List<StarWarsChapter>>()

    private val chapters : LiveData<List<StarWarsChapter>> = chaptersMutable

    init
    {
        viewModelScope.launch {
            chaptersMutable.value = localCacheRepo.getAllChapters()
        }.invokeOnCompletion {
            if(chaptersMutable.value !!.isEmpty())
                mutableDataState.value = DataStates.ERROR

            else
                mutableDataState.value = DataStates.SUCCESS
        }
    }

    fun getAllChapters() = chapters

    fun triggerAllChaptersUpdate()
    {
        viewModelScope.launch {
            chaptersMutable.value = localCacheRepo.getAllChapters()
        }.invokeOnCompletion {
            if(chaptersMutable.value !!.isEmpty())
                mutableDataState.value = DataStates.ERROR

            else
                mutableDataState.value = DataStates.SUCCESS
        }
    }

    fun getDataState() = dataState
}