package com.retroroots.phunwarecodingchallange.ui.home

import android.os.Bundle
import android.view.View
import android.view.ViewStub
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import androidx.recyclerview.widget.RecyclerView
import com.retroroots.phunwarecodingchallange.R
import com.retroroots.phunwarecodingchallange.data.common.StarWarsChapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment(R.layout.fragment_home)
{
    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        //Toolbar
        (requireActivity() as AppCompatActivity).apply {
            setSupportActionBar(view.findViewById(R.id.mainToolbar))

            setupActionBarWithNavController(this, findNavController())
        }

        val homeViewModel by viewModels<HomeViewModel>()

        //Adapter
        val adapter = HomeAdapter(requireContext(), object : HomeAdapter.OnItemClickedListener
        {
            override fun onItemClicked(starWarsChapter: StarWarsChapter)
            {
                val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(starWarsChapter)

                findNavController().navigate(action)
            }
        })

        val recyclerView = view.findViewById<RecyclerView>(R.id.recVw).apply {
            setHasFixedSize(true)

            this.adapter = adapter
        }

        val notInternetViewStub = view.findViewById<ViewStub>(R.id.noInternetVwStub)

        notInternetViewStub.inflate()

        view.findViewById<Button>(R.id.retryBtn).setOnClickListener{
            homeViewModel.triggerAllChaptersUpdate()
        }

        notInternetViewStub.visibility = View.GONE

        homeViewModel.getAllChapters().observe(viewLifecycleOwner)
        {
            if(it.isNotEmpty())
            {
                notInternetViewStub.visibility = View.GONE

                recyclerView.visibility = View.VISIBLE

                adapter.submitList(it)
            }
        }

        homeViewModel.getDataState().observe(viewLifecycleOwner)
        {
            if(it == DataStates.ERROR)
            {
                notInternetViewStub.visibility = View.VISIBLE

                recyclerView.visibility = View.GONE
            }
        }
    }
}