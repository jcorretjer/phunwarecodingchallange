package com.retroroots.phunwarecodingchallange.data.local.room

import com.retroroots.phunwarecodingchallange.data.common.StarWarsChapter
import com.retroroots.phunwarecodingchallange.data.local.cacheexpiration.CacheExpirationHelper
import com.retroroots.phunwarecodingchallange.data.local.cacheexpiration.Expiration
import com.retroroots.phunwarecodingchallange.data.local.sharedpref.CachedDateSharedPrefDao
import com.retroroots.phunwarecodingchallange.data.local.sharedpref.CachedDateSharedPrefHelper
import com.retroroots.phunwarecodingchallange.data.remote.StarWarsRepo
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class LocalCacheRepo @Inject constructor(private val dao: LocalCacheDao, private val starWarsRepo : StarWarsRepo, private val cachedDateSharedPrefHelper : CachedDateSharedPrefHelper)
{
    //If the cache is empty or expired, request data from server then update cache. Took part this strategy from the Jetpack guide to app architecture documentation.
    suspend fun getAllChapters() : List<StarWarsChapter>
    {
        var cached = dao.getAllChapters()

        val currentDate = Date()

        val currentCalendar = Calendar.getInstance()

        currentCalendar.time = currentDate

        if(CacheExpirationHelper.verifyExpiration(cachedDateSharedPrefHelper.getDateStamp(), Expiration(Calendar.MINUTE, CachedDateSharedPrefDao.EXPIRATION_DIGIT), currentCalendar))
            cached = emptyList()

        if(cached.isEmpty())
        {
            cached = starWarsRepo.getChapters()

            cachedDateSharedPrefHelper.insert(SimpleDateFormat("MM/dd/yyyy hh:mm:ss z", Locale.getDefault()).format(currentDate))

            dao.insert(cached)
        }

        return cached
    }
}