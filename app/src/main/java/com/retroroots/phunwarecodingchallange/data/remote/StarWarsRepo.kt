package com.retroroots.phunwarecodingchallange.data.remote

import com.retroroots.phunwarecodingchallange.data.remote.api.StarWarsApi
import javax.inject.Inject

class StarWarsRepo @Inject constructor(private val starWarsApi: StarWarsApi)
{
   suspend fun getChapters() = StarWarsRemoteSource(starWarsApi).getChapters()
}