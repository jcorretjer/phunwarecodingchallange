package com.retroroots.phunwarecodingchallange.data.remote.api

import com.retroroots.phunwarecodingchallange.data.common.StarWarsChapter
import retrofit2.Response
import retrofit2.http.GET

interface StarWarsApi
{
    companion object
    {
        const val BASE_URL = "https://raw.githubusercontent.com/phunware-services/dev-interview-homework/master/"
    }

    @GET("feed.json")
    suspend fun getChapters(): Response<List<StarWarsChapter>>

}