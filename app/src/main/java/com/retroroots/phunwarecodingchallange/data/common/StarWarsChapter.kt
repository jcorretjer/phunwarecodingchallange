package com.retroroots.phunwarecodingchallange.data.common

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Entity(tableName = "StarWarsChapterTable")
@Parcelize
data class StarWarsChapter(
        @PrimaryKey
        val id: Long,
        val description: String,
        val title: String,
        val timestamp: String,
        val image: String?,
        val date: String,
        @SerializedName("locationline1")
        val locationLine1: String,
        @SerializedName("locationline2")
        val locationLine2: String,
        val phone: String?
): Parcelable
