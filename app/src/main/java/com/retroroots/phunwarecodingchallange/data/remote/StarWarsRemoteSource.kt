package com.retroroots.phunwarecodingchallange.data.remote

import com.retroroots.phunwarecodingchallange.data.remote.api.StarWarsApi
import com.retroroots.phunwarecodingchallange.data.common.StarWarsChapter
import java.lang.Exception

class StarWarsRemoteSource(private val starWarsApi: StarWarsApi)
{
    suspend fun getChapters(): List<StarWarsChapter>
    {
        return try
        {
            val response = starWarsApi.getChapters()

            if(response.isSuccessful) response.body()!! else emptyList()
        }
        catch (e : Exception)
        {
            emptyList()
        }
    }
}