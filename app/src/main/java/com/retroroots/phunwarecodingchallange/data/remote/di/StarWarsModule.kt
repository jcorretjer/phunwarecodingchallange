package com.retroroots.phunwarecodingchallange.data.remote.di

import com.retroroots.phunwarecodingchallange.data.remote.api.StarWarsApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object StarWarsModule
{
    @Singleton
    @Provides
    fun  providesStarWarsApi(): StarWarsApi =
        Retrofit.Builder()
            .baseUrl(StarWarsApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(StarWarsApi::class.java)
}