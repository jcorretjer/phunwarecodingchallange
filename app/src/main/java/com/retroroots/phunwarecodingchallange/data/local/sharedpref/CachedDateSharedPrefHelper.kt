package com.retroroots.phunwarecodingchallange.data.local.sharedpref

import javax.inject.Inject

class CachedDateSharedPrefHelper @Inject constructor(private val cachedDateSharedPrefRepo : CachedDateSharedPrefRepo) : CachedDateSharedPrefDao
{
    override fun insert(cachedDateStamp : String)
    {
        cachedDateSharedPrefRepo.insert(cachedDateStamp)
    }

    override fun getDateStamp() = cachedDateSharedPrefRepo.getCacheDate()!!
}