package com.retroroots.phunwarecodingchallange.data.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.retroroots.phunwarecodingchallange.data.common.StarWarsChapter

@Database(entities = [StarWarsChapter::class], version = 1, exportSchema = false)
abstract class LocalCacheDB: RoomDatabase()
{
    abstract fun getDao(): LocalCacheDao
}