package com.retroroots.phunwarecodingchallange.data.local.sharedpref

interface CachedDateSharedPrefDao
{
    companion object
    {
        const val EXPIRATION_DIGIT = 5
    }

    fun insert(cachedDateStamp : String)

    fun getDateStamp() : String
}