package com.retroroots.phunwarecodingchallange.data.local.room.di

import android.app.Application
import androidx.room.Room
import com.retroroots.phunwarecodingchallange.data.local.room.LocalCacheDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object LocalCacheModule
{
    @Singleton
    @Provides
    fun providesDb(application: Application) =
        Room.databaseBuilder(application, LocalCacheDB::class.java, "LocalCacheDB")
            .fallbackToDestructiveMigration()
            .build()

    @Singleton
    @Provides
    fun providesLocalCacheDao(db: LocalCacheDB) = db.getDao()
}