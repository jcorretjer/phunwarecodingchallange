package com.retroroots.phunwarecodingchallange.data.local.cacheexpiration

import java.util.*

interface CacheExpiration
{
    fun verifyExpiration(cachedDateString : String, expiration : Expiration, currentCalendar : Calendar) : Boolean
}