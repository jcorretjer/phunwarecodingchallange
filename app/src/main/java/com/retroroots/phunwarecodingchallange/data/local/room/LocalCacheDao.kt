package com.retroroots.phunwarecodingchallange.data.local.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.retroroots.phunwarecodingchallange.data.common.StarWarsChapter

@Dao
interface LocalCacheDao
{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(chapter: List<StarWarsChapter>)

    @Query("SELECT * FROM StarWarsChapterTable")
    suspend fun getAllChapters(): List<StarWarsChapter>
}