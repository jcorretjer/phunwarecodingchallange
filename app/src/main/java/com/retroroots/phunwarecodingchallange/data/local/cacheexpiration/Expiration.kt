package com.retroroots.phunwarecodingchallange.data.local.cacheexpiration

data class Expiration(
    val calendarField : Int,
    val expiresIn : Int
    )
