package com.retroroots.phunwarecodingchallange.data.local.sharedpref.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object CachedDateSharedPrefModule
{
    private const val CACHED_DATE_PREFS_TAG = "CACHED_DATE_PREFS"

    @Singleton
    @Provides
    fun providesCachedDateSharedPref(application : Application) : SharedPreferences =
        application.getSharedPreferences(CACHED_DATE_PREFS_TAG, Context.MODE_PRIVATE)
}