package com.retroroots.phunwarecodingchallange.data.local.cacheexpiration

import com.retroroots.phunwarecodingchallange.data.local.sharedpref.CachedDateSharedPrefDao
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object CacheExpirationHelper : CacheExpiration
{
    /**
     * @return If the cache expired.
     */
    override fun verifyExpiration(cachedDateString : String, expiration : Expiration, currentCalendar : Calendar) =
        if(cachedDateString.isNotEmpty())
        {
            var cachedDate : Date? = null

            try
            {
                cachedDate = SimpleDateFormat("MM/dd/yyyy hh:mm:ss z", Locale.getDefault()).parse(cachedDateString) !!
            }

            catch (e : ParseException)
            {
            }

            if(cachedDate != null)
            {
                val cachedCalendar = Calendar.getInstance()

                cachedCalendar.time = cachedDate

                cachedCalendar.add(Calendar.MINUTE, CachedDateSharedPrefDao.EXPIRATION_DIGIT)

                currentCalendar >= cachedCalendar
            }

            else
                false
        }

        else
            false
}