package com.retroroots.phunwarecodingchallange.data.local.sharedpref

import android.content.SharedPreferences
import javax.inject.Inject

class CachedDateSharedPrefRepo @Inject constructor(private val sharedPreferences : SharedPreferences)
{
    companion object
    {
        private const val CACHED_DATE_PREF_KEY = "CACHED_DATE_KEY"
    }

    fun insert(cachedDate: String)
    {
        sharedPreferences.edit().putString(CACHED_DATE_PREF_KEY, cachedDate).apply()
    }

    fun getCacheDate() = sharedPreferences.getString(CACHED_DATE_PREF_KEY, "")
}