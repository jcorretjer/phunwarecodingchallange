package com.retroroots.phunwarecodingchallange.data.local.cacheexpiration

import com.retroroots.phunwarecodingchallange.data.local.sharedpref.CachedDateSharedPrefDao
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class CacheExpirationTest
{
    companion object
    {
        private const val CACHED_DATE_STRING = "07/31/2021 11:20:10 EST"
    }

    private lateinit var currentCalendar : Calendar

    @Before
    fun init()
    {
        currentCalendar = Calendar.getInstance()
    }

    @Test
    fun expiredCacheTest()
    {
        currentCalendar.time = SimpleDateFormat("MM/dd/yyyy hh:mm:ss z", Locale.getDefault()).parse(CACHED_DATE_STRING) !!

        //Current date is ahead the cache date x2 minutes.
        currentCalendar.add(Calendar.MINUTE, CachedDateSharedPrefDao.EXPIRATION_DIGIT + CachedDateSharedPrefDao.EXPIRATION_DIGIT)

        assertTrue(CacheExpirationHelper.verifyExpiration(CACHED_DATE_STRING, Expiration(Calendar.MINUTE, CachedDateSharedPrefDao.EXPIRATION_DIGIT), currentCalendar))
    }

    @Test
    fun cacheStillGoodTest()
    {
        currentCalendar.time = SimpleDateFormat("MM/dd/yyyy hh:mm:ss z", Locale.getDefault()).parse(CACHED_DATE_STRING) !!

        //Current date is behind the cache date by one minute.
        currentCalendar.add(Calendar.MINUTE, CachedDateSharedPrefDao.EXPIRATION_DIGIT - 1)

        assertFalse(CacheExpirationHelper.verifyExpiration(CACHED_DATE_STRING, Expiration(Calendar.MINUTE, CachedDateSharedPrefDao.EXPIRATION_DIGIT), currentCalendar))
    }

    @Test
    fun noCachedDateTest()
    {
        currentCalendar.time = Date()

        assertFalse(CacheExpirationHelper.verifyExpiration("", Expiration(Calendar.MINUTE, CachedDateSharedPrefDao.EXPIRATION_DIGIT), currentCalendar))
    }

    @Test
    fun invalidDateStringFormatTest()
    {
        currentCalendar.time = Date()

        assertFalse(CacheExpirationHelper.verifyExpiration("%", Expiration(Calendar.MINUTE, CachedDateSharedPrefDao.EXPIRATION_DIGIT), currentCalendar))
    }
}