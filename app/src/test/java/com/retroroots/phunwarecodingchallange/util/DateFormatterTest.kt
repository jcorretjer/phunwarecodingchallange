package com.retroroots.phunwarecodingchallange.util

import com.retroroots.phunwarecodingchallange.util.dateFormatter.DateFormat
import com.retroroots.phunwarecodingchallange.util.dateFormatter.DateFormatter
import org.junit.Assert.assertEquals
import org.junit.Test

class DateFormatterTest
{
    @Test
    fun formatDateWithEmptyCurrentFormatAndFailTest()
    {
        try
        {
            DateFormatter.format(DateFormat())

            throw Exception("Failed Test")
        }

        catch (e: Exception)
        {
            //If it catches an error than it passed because this is what we were expecting
            assertEquals(DateFormatter.ExceptionMessages.EMPTY_CURRENT_FORMAT, e.message)
        }
    }

    @Test
    fun formatDateWithEmptyOutputFormatAndFailTest()
    {
        try
        {
            DateFormatter.format(DateFormat(currentFormat = "yyyy-MM-dd HH:mm:ss"))

            throw Exception("Failed Test")
        }

        catch (e: Exception)
        {
            //If it catches an error than it passed because this is what we were expecting
            assertEquals(DateFormatter.ExceptionMessages.EMPTY_OUTPUT_FORMAT, e.message)
        }
    }

    @Test
    fun formatDateWithEmptyDateAndFailTest()
    {
        try
        {
            DateFormatter.format(DateFormat(currentFormat = "yyyy-MM-dd HH:mm:ss", outputFormat = "MMM dd, yyyy h:mm z"))

            throw Exception("Failed Test")
        }

        catch (e: Exception)
        {
            //If it catches an error than it passed because this is what we were expecting
            assertEquals(DateFormatter.ExceptionMessages.EMPTY_DATE, e.message)
        }
    }

    @Test
    fun formatDateWithCorrectArgumentsTest()
    {
        assertEquals("Jun 18, 2015 11:30 EDT", DateFormatter.format(DateFormat(currentFormat = "yyyy-MM-dd HH:mm:ss", outputFormat = "MMM dd, yyyy h:mm z", date = "2015-06-18T23:30:00.000Z")))
    }
}